import {Repository, Inject} from '../configuration/noants/decorators';
import { UserEntity } from './user.entity';
import { ServerConfiguration } from '../configuration/noants/server.configuration';

@Repository
export class UserRepository {

    @Inject serverConfiguration: ServerConfiguration;

    constructor() {};
    
    getUserByUid(uid: string): Promise<UserEntity> {
        return new Promise((resolve, reject) => {
            this.searchUser(uid, async (err, res) => {
                let user: UserEntity = null;
                res.on('searchEntry', (ldapUser) => {
                    try {
                        if (!user) {
                            user = new UserEntity(ldapUser.object.uid, ldapUser.object.uid, ldapUser.object.mail[0], ldapUser.object.name, null);
                        } else { 
                            throw new Error('More than one user by UID: ' + uid);
                        }
                    } catch(ex) {
                        reject(ex);
                    }
                });
                res.on('end', () => {
                    if (user) {
                        resolve(user);
                    } else {
                        reject(new Error('User not found by UID: ' + uid));
                    }
                })
                res.on('error', function(err) {
                    reject(err);
                });
            })
        })
    }
    async getUserByUidAndPassword(uid: string, password: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.searchUser(uid, (err, res) => {
                let found = false;
                if (err) {
                    reject(err);
                }
                res.on('searchEntry', async (entry) => {
                    if (!found) {
                        found = true;
                        try {
                            resolve(await this.performAuthentication(entry, password));
                        } catch(ex) {
                            reject(ex);
                        }
                    } else {
                        console.error('Was found more than one from LDAP.');
                        reject('Was found more than one from LDAP.');
                    }
                });
                res.on('end', () => {
                    if (!found){
                        reject('No user found.');
                    }
                })
                res.on('error', (err) => {
                    reject(err);
                });
            })
        })
    }
    async performAuthentication(ldapUser: any, password: string) {
        return new Promise((resolve, reject) => {
            this.serverConfiguration.ldapClient.bind(ldapUser.object.dn, password, async (err) => {
                if (err) {
                    reject(err);
                }
                try {
                    const user = new UserEntity(ldapUser.object.uid, ldapUser.object.uid, ldapUser.object.mail[0], ldapUser.object.name, null);
                    resolve(user);
                } catch(ex) {
                    reject(ex);
                }
        })});
    }
    async searchUser(uid: any, cb: Function) {
        let filter = {
            scope: 'sub',
            filter: '(&(uid=' + uid + ')(objectclass=person))'
        }
        this.serverConfiguration.ldapClient.search('dc=gvt,dc=net,dc=br', filter, cb);
    }
}
