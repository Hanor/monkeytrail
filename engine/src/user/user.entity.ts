export class UserEntity  {
    constructor(
        private id: number,
        private username: string,
        private email: string,
        private name: string,
        private password: string
    ) {};

    getId(): number {
        return this.id;
    }
    getUserName(): string {
        return this.username;
    }
    getEmail(): string {
        return this.email; 
    }
    getPassword(): string {
        return this.password;
    }
}