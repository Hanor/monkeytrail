import {Rest, Get, Inject, Post} from '../configuration/noants/decorators';

import { UserService } from './user.service';
import { UserEntity } from './user.entity';
import { AuthenticationService } from '../authentication/authentication.service';

@Rest
export class UserRest {

  @Inject userService: UserService;
  @Inject authenticationService: AuthenticationService

  @Post(['/api/signIn'])
  async signIn(signInForm: any): Promise<any> {
    return await this.authenticationService.doSignIn(signInForm.login, signInForm.password);
  }

  @Get(['/api/signOut/:username', true])
  signOut(username: string): any {
    return this.authenticationService.doSignOut(username);
  }

  @Get(['/api/signedUser/:username', true])
  async signedUser(username: string): Promise<UserEntity> {
    return await this.userService.getUserByUid(username);
  }
}
