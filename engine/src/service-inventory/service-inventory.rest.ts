import {Rest, Get, Inject} from '../configuration/noants/decorators';
import { ServerConfiguration } from '../configuration/noants/server.configuration';
import { ServiceInventoryService } from './service-inventory.service';

@Rest
export class ServiceInventoryRest {
    @Inject serviceInventoryService: ServiceInventoryService;

    @Get(['/api/getServiceInventoryItemByDesignator', true]) 
    async getServiceInventoryItemByDesignator(query: any): Promise<any> {
        try {
            return await this.serviceInventoryService.getAccountItemsByDesignator(query);
        } catch (ex) {
            throw ex;
        }
    }
}

// x = {
//     "return": {
//         "returnCode": 0,
//         "accounts": {
//             "externalId": "8-394302979401",
//             "accountType": 1,
//             "address": {
//                 "externalId": "8-394302979476",
//                 "items": [
//                     {
//                         "designator": {
//                             "designatorType": 10,
//                             "value": "03092278",
//                             "status": 1,
//                             "statusName": null
//                         },
//                         "id": 150941791,
//                         "specId": 15,
//                         "specIdName": "3rd Partner Access",
//                         "status": 1,
//                         "statusName": "PENDING",
//                         "createdDate": "2018-12-20T20:16:32.000Z",
//                         "modifiedDate": "2018-12-20T20:16:32.000Z",
//                         "items": {
//                             "designator": {
//                                 "designatorType": 1,
//                                 "value": "C0047D651C",
//                                 "status": 1,
//                                 "statusName": null
//                             },
//                             "id": 150941792,
//                             "specId": 16,
//                             "specIdName": "Serviços Digitais",
//                             "status": 1,
//                             "statusName": "PENDING",
//                             "createdDate": "2018-12-20T20:16:32.000Z",
//                             "modifiedDate": "2018-12-20T20:16:32.000Z",
//                             "param": {
//                                 "name": "CorrelationId",
//                                 "value": "SJC-81530HYUS6-013",
//                                 "createdDate": "2018-12-20T20:16:32.000Z",
//                                 "modifiedDate": "2018-12-20T20:16:32.000Z"
//                             }
//                         },
//                         "param": [
//                             {
//                                 "name": "NetworkOwner",
//                                 "value": "VIVO1",
//                                 "createdDate": "2018-12-20T20:16:32.000Z",
//                                 "modifiedDate": "2018-12-20T20:16:32.000Z"
//                             },
//                             {
//                                 "name": "TipoAcesso",
//                                 "value": "WIRELESS",
//                                 "createdDate": "2018-12-20T20:16:32.000Z",
//                                 "modifiedDate": "2018-12-20T20:16:32.000Z"
//                             }
//                         ]
//                     },
//                     {
//                         "designator": {
//                             "designatorType": 1,
//                             "value": "SJC-30285150-069",
//                             "status": 1,
//                             "statusName": null
//                         },
//                         "id": 150941788,
//                         "specId": 6,
//                         "specIdName": "Wired Access",
//                         "status": 1,
//                         "statusName": "PENDING",
//                         "createdDate": "2018-12-20T20:16:26.000Z",
//                         "modifiedDate": "2018-12-20T20:16:26.000Z",
//                         "items": [
//                             {
//                                 "designator": {
//                                     "designatorType": 2,
//                                     "value": "1239026664",
//                                     "status": 1,
//                                     "statusName": null
//                                 },
//                                 "id": 150941789,
//                                 "specId": 3,
//                                 "specIdName": "Linha Telefônica",
//                                 "status": 1,
//                                 "statusName": "PENDING",
//                                 "createdDate": "2018-12-20T20:16:26.000Z",
//                                 "modifiedDate": "2018-12-20T20:24:09.000Z",
//                                 "param": [
//                                     {
//                                         "name": "RPON",
//                                         "value": "C0047D651A",
//                                         "createdDate": "2018-12-20T20:24:09.000Z",
//                                         "modifiedDate": "2018-12-20T20:24:09.000Z"
//                                     },
//                                     {
//                                         "name": "StatusBloqueio",
//                                         "value": "UNBLOCKED",
//                                         "createdDate": "2018-12-20T20:24:09.000Z",
//                                         "modifiedDate": "2018-12-20T20:24:09.000Z"
//                                     },
//                                     {
//                                         "name": "TecnologiaVoz",
//                                         "value": "SIP",
//                                         "createdDate": "2018-12-20T20:16:27.000Z",
//                                         "modifiedDate": "2018-12-20T20:16:27.000Z"
//                                     }
//                                 ]
//                             },
//                             {
//                                 "designator": {
//                                     "designatorType": 3,
//                                     "value": "SJC-81530HYUS6-013",
//                                     "status": 1,
//                                     "statusName": null
//                                 },
//                                 "id": 150941790,
//                                 "specId": 4,
//                                 "specIdName": "Banda Larga",
//                                 "status": 1,
//                                 "statusName": "PENDING",
//                                 "createdDate": "2018-12-20T20:16:27.000Z",
//                                 "modifiedDate": "2018-12-20T20:24:09.000Z",
//                                 "param": [
//                                     {
//                                         "name": "CorrelationId",
//                                         "value": "1239026664",
//                                         "createdDate": "2018-12-20T20:24:09.000Z",
//                                         "modifiedDate": "2018-12-20T20:24:09.000Z"
//                                     },
//                                     {
//                                         "name": "Downstream",
//                                         "value": "10240",
//                                         "createdDate": "2018-12-20T20:16:27.000Z",
//                                         "modifiedDate": "2018-12-20T20:24:09.000Z"
//                                     },
//                                     {
//                                         "name": "RPON",
//                                         "value": "C0047D651C",
//                                         "createdDate": "2018-12-20T20:24:09.000Z",
//                                         "modifiedDate": "2018-12-20T20:24:09.000Z"
//                                     },
//                                     {
//                                         "name": "StatusBloqueio",
//                                         "value": "UNBLOCKED",
//                                         "createdDate": "2018-12-20T20:24:09.000Z",
//                                         "modifiedDate": "2018-12-20T20:24:09.000Z"
//                                     },
//                                     {
//                                         "name": "Upstream",
//                                         "value": "1024",
//                                         "createdDate": "2018-12-20T20:16:27.000Z",
//                                         "modifiedDate": "2018-12-20T20:24:09.000Z"
//                                     }
//                                 ]
//                             }
//                         ],
//                         "param": [
//                             {
//                                 "name": "NRC",
//                                 "value": "C0047D651A",
//                                 "createdDate": "2018-12-20T20:16:26.000Z",
//                                 "modifiedDate": "2018-12-20T20:16:26.000Z"
//                             },
//                             {
//                                 "name": "NetworkOwner",
//                                 "value": "VIVO1",
//                                 "createdDate": "2018-12-20T20:16:26.000Z",
//                                 "modifiedDate": "2018-12-20T20:16:26.000Z"
//                             },
//                             {
//                                 "name": "NetworkOwnerId",
//                                 "value": "115638452012504",
//                                 "createdDate": "2018-12-20T20:16:26.000Z",
//                                 "modifiedDate": "2018-12-20T20:16:26.000Z"
//                             },
//                             {
//                                 "name": "TipoAcesso",
//                                 "value": "METALICO",
//                                 "createdDate": "2018-12-20T20:16:26.000Z",
//                                 "modifiedDate": "2018-12-20T20:16:26.000Z"
//                             }
//                         ]
//                     }
//                 ]
//             }
//         }
//     }
// }