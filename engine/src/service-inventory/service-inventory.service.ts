import { Service, Inject } from "../configuration/noants/decorators";
import { ServerConfiguration } from "../configuration/noants/server.configuration";

@Service
export class ServiceInventoryService {

    @Inject serverConfiguration: ServerConfiguration;

    async getAccountItemsByDesignator(query): Promise<any> {
        let method;
        if (query.environment === 'prod') {
            method = this.serverConfiguration.serviceInventoryProd['getAccountItems'];
        } else {
            method = this.serverConfiguration.serviceInventoryQa3['getAccountItems'];
        }

        return new Promise((resolve, reject) => {
            method({designator: query.designator}, async (err, result) => {
                if (err) {
                    reject(err)
                }
                resolve(result);
            })
        })
    }
}