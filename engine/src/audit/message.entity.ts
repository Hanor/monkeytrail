import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;
const MessageEntity = new Schema({
    activateResourceIn: String,
    activateResourceInActivity: String,
    errorsMessages: Array,
    correlationsMessagesIds: Array,
    raConnectorIteraction: String,
    date: Date,
    activity: String,
    externalId: String,
    notifications: Array,
    responses: Array,
    isCorrelation: Boolean,
    messageId: String,
    requests: Array
});
export const MessageModel = mongoose.model('MessageEntity', MessageEntity )