import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;
const AuditProcessEntity = new Schema({
    filesAmount: Number,
    filesProcessed: Number,
    minDateFile: Date,
    maxDateFile: Date,
    initDate: Date,
    finalDate: Date
});
const AuditProcessFileEntity = new Schema({
    name: String,
    moved: Boolean,
    completed: Boolean,
    compressed: Boolean,
    dateFile: Date
});
export const AuditProcessFileModel = mongoose.model('AuditProcessFileEntity', AuditProcessFileEntity)
export const AuditProcessModel = mongoose.model('AuditProcessEntity', AuditProcessEntity)