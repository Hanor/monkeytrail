import {Repository} from '../configuration/noants/decorators';
import { MessageModel } from './message.entity';

@Repository
export class AuditRepository {
    cache: any = {};
    saved: string = '';
    async getAllExternalIds(): Promise<any> {
        return MessageModel.aggregate([{$match:{externalId:{$regex:/8\-.*\-/i}}},{$group: {_id: "$externalId"}}]);
    }
    async findMessagesByExternalIdAndActivities(externalId: string, activities: Array<string>): Promise<any> {
        let messages = [];
        for (let activity of activities) {
            let result = (await (MessageModel.findOne({externalId: externalId, activateResourceInActivity: activity})));
            if (result) {
                messages.push(result);
            }
        }
        return messages;
    }
    async findMessagesByMessageId(messagesIds: Array<string>): Promise<any> {
        let messages = [];
        for (let messageId of messagesIds) {
            let result = (await (MessageModel.findOne({messageId: messageId})));
            if (result) {
                messages.push(result);
            }
        }
        return messages;
    }
    async getMessagesByExternalId(externalId: string, isCorrelation: boolean): Promise<any> {
        let query: any = {externalId:externalId};
        if (isCorrelation != undefined) {
            query.isCorrelation = isCorrelation;
        }
        return await (MessageModel.find(query).sort({date: -1}));
    }
    async countRequestsByExternalId(externalId: string, isCorrelation: boolean) {
        let query: any = {externalId:externalId};
        if (isCorrelation != undefined && isCorrelation != null) {
            query.isCorrelation = isCorrelation;
        }
        return await MessageModel.countDocuments(query);
    }
    async countRequestsByExternalIdAndActivityName(externalId: string, activityName: string, isCorrelation: boolean) {
        let query: any = {externalId: externalId, activateResourceInActivity: activityName, "errorsMessages.0": {$exists: true}}
        if (isCorrelation) {
            query.isCorrelation = isCorrelation;
        }
        return await MessageModel.countDocuments(query);
    }
    async getMessageById(id: string): Promise<any> {
        return await MessageModel.findOne({messageId: id});
    }
    async getMessageByExternalIdAndActivityNameAndHasError(externalId: string, activityName: string, isCorrelation: boolean) {
        let query: any = {externalId: externalId, activateResourceInActivity: activityName, "errorsMessages.0": {$exists: true}}
        if (isCorrelation) {
            query.isCorrelation = isCorrelation;
        }
        return await MessageModel.find(query).sort({"date": -1}).limit(1);
    }
    async getMessageFromCache(messageId: string, fileName: string): Promise<any> {
        let cacheItem: CacheItem = this.cache[messageId];
        if (cacheItem) {
            cacheItem.using.add(fileName);
            return cacheItem.message;
        } else {
            return null;
        }
    }
    async getMessageFromCacheOrDatabase(messageId: string, fileName: string): Promise<any> {
        let message = await this.getMessageFromCache(messageId, fileName);
        if (!message) {
            message = await MessageModel.findOne({messageId: messageId});
            if (message) {
                this.setInCache(message, fileName);
            }
        }
        return message;
    }
    async saveMessage(message: any, fileName: string): Promise<any> {
        let item: CacheItem = this.cache[message.messageId];
        try {
            if (item) {
                item.using.delete(fileName);
                if (item.using.size === 0) {
                    if (!item.isSaving) {
                        item.isSaving = true;
                        await message.save();
                        item.isSaving = false;
                        if (item.using.size === 0) {
                            delete this.cache[message.messageId];
                        }
                    }
                }
            }
            else {
                throw new Error('Saving a message not present in the cache!');
            }
        } catch (ex) {
            console.log("Message:" + message.messageId);
            if (ex.toString().includes('[ERR_BUFFER_OUT_OF_BOUNDS]: Attempt to write outside buffer bounds')) {
                delete this.cache[message.messageId];
            }
            throw ex;
        }
    }
    async setInCache(message: any, fileName: string): Promise<void> {
        let item: CacheItem = this.cache[message.messageId];
        if(!item) {
            item = this.cache[message.messageId] = new CacheItem();
            item.using.add(fileName);
            item.message = message;
        }
    }
    async reset(): Promise<void> {
        this.cache = {};
    }
}

class CacheItem {
    using: Set<string> = new Set();
    message: any;
    isSaving: boolean = false;
}