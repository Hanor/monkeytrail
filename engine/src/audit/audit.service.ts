import {Service, Inject} from '../configuration/noants/decorators';
import { AuditRepository } from './audit.repository';
import { AuditProcessModel, AuditProcessFileModel } from './audit-process.entity';
import { FileProcessService } from '../file/file-process.service';
import { AuditProcess } from './audit-process';
import { ServiceInventoryService } from '../service-inventory/service-inventory.service';

@Service
export class AuditService {
    static LOG_FILES = './engine/log_files/';

    @Inject auditRepository: AuditRepository;
    @Inject fileProcessService: FileProcessService;
    @Inject auditProcess: AuditProcess;
    @Inject serviceInventoryService: ServiceInventoryService;
    
    async getOwnerIdAndInstanceByExternalIds(audit: any): Promise<any> {
        let csv = "ORDEM;NETWORK_OWNER_ID;INSTANCE\n";

        let externalIds: Array<string> = <Array<string>> audit.externalIds;
        for(let externalId of externalIds) {
            let messages = await this.auditRepository.findMessagesByExternalIdAndActivities(externalId, 
                ['CONFIGURE_AUTHENTICATION', 'CONFIGURE_VOICE_BRIDGE', 'CONFIGURE_SWITCH_VAS']);
            let instance = '';
            let networkOwnerId = '';
            for(let message of messages) {
                if (message.activateResourceInActivity === 'CONFIGURE_AUTHENTICATION') {
                    let activateResourceIn = message.activateResourceIn;
                    let min = activateResourceIn.search('<name>NETWORK_OWNER_ID</name><value>');
                    let finalKey = '';
                    for(let i  = min + "<name>NETWORK_OWNER_ID</name><value>".length; min < activateResourceIn.length; i++) {
                        finalKey += activateResourceIn[i];
                        if (finalKey.includes('</value>')) {
                            networkOwnerId = finalKey.replace('</value>', '');
                            break;
                        }
                    }
                } else {
                    let activateResourceIn = message.activateResourceIn;
                    let min = activateResourceIn.search(/\<serviceId\>.*/);
                    if (min > -1) {
                        let max = activateResourceIn.search(/\<\/serviceId\>/);
                        instance = activateResourceIn.substring(min, max).replace("<serviceId>", "").trim();
                    }
                }
            }
            if (networkOwnerId === '' && instance != '') {
                try {
                    let response = (await this.serviceInventoryService.getAccountItemsByDesignator({designator: instance, environment: 'prod'}));
                    let accountItems = response.return.accounts;
                    if (!accountItems) {
                        networkOwnerId = "Não encontrado no IS.";
                    } else {
                        let addressList = accountItems.address;
                        if (accountItems.length > 0) {
                            console.log("WTF: " + instance);
                        }
                        if (addressList instanceof Array) {
                            for (let address of addressList) {
                                networkOwnerId = this.getNetworkOwnerIdFromAddress(address);
                                if (networkOwnerId && networkOwnerId != "") {
                                    console.log(networkOwnerId);
                                    break;
                                }
                            }
                        } else {
                            networkOwnerId = this.getNetworkOwnerIdFromAddress(addressList);
                        }
                    }
                } catch(ex) {
                    networkOwnerId = 'Não encontrado.'
                    console.log(ex);
                }

            }
            if (messages.length === 0) {
                networkOwnerId = "Não passou pelo RA.";
            }
            csv += externalId + ';'+ networkOwnerId + ';'+ instance +'\n';
        }
        return csv;
    }
    async getAllExternalIds(): Promise<any> {
        return this.auditRepository.getAllExternalIds();
    }
    getNetworkOwnerIdFromAddress(address) {
        let items = [];
        if (address.items instanceof Array) {
            items = address.items;
        } else {
            items = [address.items];
        }

        for(let item of items) {
            for (let param of item.param) {
                if (param.name === 'NetworkOwnerId' && param.value.length == 15) {
                    return param.value;
                }
            }
        }
    }
    private async afterExecuteFilesProcess(auditProcess: any): Promise<any>{
        
        await this.auditProcess.reset();
        this.auditProcess.maxExecutions = 6;
        this.auditProcess.files = (await this.fileProcessService.getFilesNotCompleted());

        await this.auditProcess.startProcess(auditProcess);
        return auditProcess;
    }
    async auditExternalIdAndActivities(externalId: any, activities: Set<string>, countRequests: number, correlation: boolean): Promise<any >{
        let messages = [];
        let countByActivity = {};
        for(let activity of activities) {
            countByActivity[activity] = await this.auditRepository.countRequestsByExternalIdAndActivityName(externalId, activity, correlation);
            if (countByActivity[activity] > 0) {
                messages.push((await this.auditRepository.getMessageByExternalIdAndActivityNameAndHasError(externalId, activity, correlation))[0]);
            }
        }
        return {externalId: externalId, countRequests: countRequests, messages: messages, countByActivity: countByActivity};
    }
    async auditExternalIds(audit: any): Promise<any> {
        
        if (!audit) {
            return [];
        }

        let externalIds: Array<string> = <Array<string>> audit.externalId;
        let activities: Array<string> = <Array<string>> audit.activities;
        let externalIdFound = [];
        for(let externalId of externalIds) {
            let countRequests = await this.auditRepository.countRequestsByExternalId(externalId, audit.correlation);
            if (!activities || activities.length === 0) {
                let messages = await this.auditRepository.getMessagesByExternalId(externalId, audit.correlation);
                externalIdFound.push({externalId: externalId, messages: messages, countRequests: countRequests});
            } else {
                externalIdFound.push(await this.auditExternalIdAndActivities(externalId, audit.activities, countRequests, audit.correlation));
            }
        }
        return externalIdFound;
    }
    async auditExternalId(query: any): Promise<any> {
        let externalId: string = query.externalId;
        let initDate: Date = query.initDate;
        let finalDate: Date = query.finalDate;
        return await this.auditRepository.getMessagesByExternalId(externalId, null);
    }
    private async createProcessIfHasFileNotCompleted(): Promise<any> {
        let filesNotCompleted = await AuditProcessFileModel.countDocuments({completed: false});
        if (filesNotCompleted > 0) {
            let auditProcess = new AuditProcessModel();
            auditProcess.initDate = new Date();
            auditProcess.minDateFile = null;
            auditProcess.maxDateFile = null;
            auditProcess.filesAmount = filesNotCompleted;
            auditProcess.filesProcessed = 0;
            await auditProcess.save();
            return auditProcess;
        }
        return null;

    }
    private async executeFilesCheckProcess(path: string, fromDate: Date): Promise<any> {
        console.log('\nInitializing: FilesCheck auditProcess...')
        await this.fileProcessService.checkFilesToBeProcessed(path, fromDate);
        console.log('FilesCheck COMPLETED!')
        if (this.fileProcessService.files.length > 0) {
            let auditProcess = new AuditProcessModel();
            auditProcess.initDate = new Date();
            auditProcess.minDateFile = this.fileProcessService.minDate;
            auditProcess.maxDateFile = this.fileProcessService.maxDate;
            auditProcess.filesAmount = this.fileProcessService.files.length;
            auditProcess.filesProcessed = 0;
            await auditProcess.save();
            return auditProcess;
        } 
        return null;
    }
    private async executeFilesContentProcess(auditProcess: any) {
        console.log('\nInitializing: FileContent auditProcess...')
        await this.afterExecuteFilesProcess(auditProcess);
        auditProcess.finalDate = new Date();
        await auditProcess.save();
        this.fileProcessService.files = [];
        console.log('FilesContent COMPLETED!')
    }
    private async executeFilesProcess(auditProcess: any) {
        try {
            console.log('Initializing: FilesProcess...')
            console.log('Files to be processed: ' + this.fileProcessService.files.length)
            await this.fileProcessService.extractAndMoveFiles(auditProcess);
            await auditProcess.save();
            console.log('FilesProcess COMPLETED!')
            await this.executeFilesContentProcess(auditProcess);
        } catch (ex) {
            this.fileProcessService.files = [];
            throw ex;
        }
    }
    async findMessagesByMessageId(messageIds: Array<string>): Promise<any> {
        return await this.auditRepository.findMessagesByMessageId(messageIds);
    }
    async startFilesProcess(path: string): Promise<string> {
        try {
            if (this.auditProcess.processingFiles > 0) {
                return 'Proccess is running';
            }

            let fromDate = new Date(2018, 10, 20);
            this.fileProcessService.files = [];
            let auditProcess = await this.executeFilesCheckProcess(path, fromDate);
            if (!auditProcess) {
                let auditProcess = await this.createProcessIfHasFileNotCompleted();
                if(!auditProcess) {
                    return 'No file to be processed';
                } else {
                    this.executeFilesContentProcess(auditProcess);
                    return 'Proccess initialized - From local database';
                }
            } else {
                this.executeFilesProcess(auditProcess);
                return 'Proccess initialized';
            }
        } catch (ex) {
            this.fileProcessService.files = [];
            throw ex;
            
        }
    }
}
