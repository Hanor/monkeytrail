import {Rest, Get, Inject, Post} from '../configuration/noants/decorators';

import { AuditService } from './audit.service';

@Rest
export class AuditRest {

    @Inject auditService: AuditService;

    @Get(['/api/startFilesProcess/:path', true])
    async process(path: string): Promise<any> {
        return await {started: await this.auditService.startFilesProcess(path)};
    }

    @Get(['/api/findMessagesByMessageIds', true])
    async findMessagesByMessageId(query: any): Promise<any> {
        if (!(query.messagesIds instanceof Array)) {
            query.messagesIds = [query.messagesIds];
        }
        let externalsIds = await this.auditService.findMessagesByMessageId(query.messagesIds);
        return externalsIds
    }

    @Get(['/api/auditExternalId', true])
    async auditExternalId(query: any): Promise<any> {
        return await this.auditService.auditExternalId(query);
    }
    
    @Get(['/api/getAllExternalIds', true])
    async getAllExternalIds(): Promise<any> {
        return await this.auditService.getAllExternalIds();
    }

    @Post(['/api/auditExternalIds', true])
    async auditExternalIds(audit: any): Promise<any> {
        return await this.auditService.auditExternalIds(audit);
    }

    @Post(['/api/getOwnerIdAndInstanceByExternalIds', true]) 
    async getOwnerIdAndInstanceByExternalIds(audit: any): Promise<any> {
        return await this.auditService.getOwnerIdAndInstanceByExternalIds(audit);
    }
}
