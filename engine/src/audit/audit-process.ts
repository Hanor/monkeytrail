import { FileProcessService } from '../file/file-process.service';
import { Inject } from '../configuration/noants/decorators';
import { BehaviorSubject, Subscription } from 'rxjs';
import { AuditService } from './audit.service';
import { AuditRepository } from './audit.repository';
import { MessageModel } from './message.entity';

export class AuditProcess {

    @Inject fileProcessService: FileProcessService;
    @Inject auditRepository: AuditRepository;
    
    fileDone$: BehaviorSubject<string> = new BehaviorSubject(null);
    subscriptions$: Subscription = new Subscription();
    monitoring: any;

    currentFileIndex: number = 0;
    processingFiles: number = 0;
    processedFiles: number = 0;
    maxExecutions: number = 0;
    files: Array<string>;

    eventFileWasProcessed(process: any): void {
        this.subscriptions$.add(this.fileDone$.subscribe(async (file) => {
            if (file) {
                try {
                    this.processingFiles--;
                    if (this.currentFileIndex < this.files.length) {
                        await this.processFile(this.files[this.currentFileIndex], process);
                    } else if (this.processingFiles === 0) {
                        return;
                    }
                } catch (ex) {
                    console.error(ex);
                }
            }
        }, async (ex) => {
            console.error(ex);
            this.subscriptions$.unsubscribe();
            this.reset();
        }))
    }
    async processFile(file: any, process: any): Promise<void> {
        this.processingFiles++;
        this.currentFileIndex++;
        let single = async (process: any) => {
            let fileContent = await this.fileProcessService.getFileContent(AuditService.LOG_FILES + '/' + file.name);
            if (fileContent) {
                await this.processFileContent(fileContent.split('\n'), file, process);
            } else {
                console.error('Não encontrado o arquivo: ' + file.name);
            }
            this.wasProcessed(process, file);
        }
        await single(process);
        return;
    }
    async processFileContent(lines: Array<string>, processedFile: any, process: any): Promise<void> {
        try {
            let fileDate: Date = processedFile.dateFile;
            let executionMessage = {message: null, messageId: null};
            let fileExecution = {index: 0, lines: lines};
            for (fileExecution.index = 0; fileExecution.index < fileExecution.lines.length; fileExecution.index++) {
                let line = fileExecution.lines[fileExecution.index]
                await this.processLine(line, fileDate, executionMessage, fileExecution, processedFile.name);
            }
            if (executionMessage.message) {
                await this.auditRepository.saveMessage(executionMessage.message, processedFile.name);
            }
            processedFile.completed = true;
            await processedFile.save();
        } catch (ex) {
            console.error(ex);
        } finally {
          return;
        }
    }
    async startProcess(process: any): Promise<void> {
        this.eventFileWasProcessed(process);
        while(this.processingFiles <= this.maxExecutions && this.currentFileIndex < this.files.length) {
            this.processFile(this.files[this.currentFileIndex], process);
        }
        this.monitoring = setInterval(() => {
            console.log('-------------------------\nIn execution: ' + this.processingFiles + '\n Max executions: ' + this.maxExecutions + '-----------------\n')
        }, 5000)
    }
    async reset(): Promise<void> {
        this.currentFileIndex = 0;
        this.processingFiles = 0;
        this.subscriptions$.unsubscribe();
        this.subscriptions$ = new Subscription();
        this.processedFiles = 0;
        this.auditRepository.reset();
    }
    async createOrRetrieveMessage(messageId: string, executionMessage: any, messageDate: Date,  fileDate: Date, fileName: string) {
        try {
            if (executionMessage.message) {
                await this.auditRepository.saveMessage(executionMessage.message, fileName);
            }
            executionMessage.message = await this.getMessageByMessageIdFromLine(fileDate, messageDate, messageId, fileName);
            executionMessage.message.messageId = executionMessage.message.messageId;
        } catch (ex) {
            throw ex;
        }
    }
    async getMessageDateFromLine(line: string, fileDate: Date): Promise<Date> {
        let time = line.match(/\[[0-9]{2}\:[0-9]{2}\:[0-9]{2}\.[0-9]{3}\]/gi)[0].replace(/(\[)|(\])|(\.[0-9]{3})/g, '');
        let date = new Date(fileDate);

        date.setHours(parseInt(time.split(':')[0]));
        date.setMinutes(parseInt(time.split(':')[1]));
        date.setSeconds(parseInt(time.split(':')[2]));
        return date;
    }
    async getLineInformations(executionMessage: any, line: string, fileExecution: any) {
        await this.processLineGetOrder(line, executionMessage);
        await this.processLineGetActivateResourceIn(line, executionMessage);
        await this.processLineGetCorrelations(line, executionMessage);

        let found = await this.processLineGetRaConnectorIteraction(line, executionMessage, fileExecution);
        if (!found) {
            found = await this.processLineGetRequestOrAsyncResponse(line, executionMessage, fileExecution);
            if (!found) {
                found = await this.processLineGetNotification(line, executionMessage, fileExecution);
                if (!found) {
                    await this.processLineGetErrors(line, executionMessage, fileExecution);
                }
            }
        }
    }
    async getLineMessageId(fileExecution: any, index: number): Promise<string> {
        if (index >= fileExecution.lines.length) {
            return 'No More Lines'
        }
        let line = fileExecution.lines[index];
        let lineMessage = line.match(/\[clientId.*messageId.*\]/);
        if (lineMessage) {
            return lineMessage[0].split('messageId=')[1].split(']')[0];
        } else {
            return 'Not Found'
        }
    }
    async getMessageByMessageIdFromLine(fileDate: Date, messageDate: Date, messageId: string, fileName: string): Promise<any> {
        let fromDatabase = await this.auditRepository.getMessageFromCacheOrDatabase(messageId, fileName);
        if (fromDatabase) {
            if (fromDatabase.date > messageDate) {
                fromDatabase.date = messageDate;
            }
            return fromDatabase;
        }

        let message = new MessageModel();
        message.messageId = messageId;
        message.date = messageDate;
        message.isCorrelation = false
        await this.auditRepository.setInCache(message, fileName);
        return message;
    }
    async getTag(keys: Array<string>, line: string): Promise<string> {
        for(let key of keys) {
            let endKey = key.replace('<', '</');
            let initKeyIndex = line.search(key);
            let endKeyIndex = line.search(endKey);
            if (initKeyIndex > -1 && endKeyIndex > -1) {
                return line.substring(initKeyIndex + key.length, endKeyIndex);
            }
        }
        return null;
    }
    async hasCorrelationId(id: string, executionMessage: any): Promise<boolean> {
        for (let correlationId of executionMessage.message.correlationsMessagesIds) {
            if (id === correlationId) {
                return true;
            }
        }
        return false;
    }
    async hasError(line: string): Promise<boolean> {
        let have = /error|erro/i;
        let notHave = /(error \-\> null)|(errorcode\>0)|(Erro oriundo do equipamento\/plataforma)|(Não foi possivel recuperar a descrição de serviceRequest)|(success\=true)|(Erro ao .* a auditoria: Erro ao fazer chamada rest)/ig;

        if (line.search(have) >= 0 && line.search(notHave) === -1) {
            return true;
        }
        return false;
    }
    async isActualMessageIdDiferent(message: any, messageId: string): Promise<boolean> {
        return !message || messageId !== message.messageId
    }
    async processLine(line: string, fileDate: Date, executionMessage: any, fileExecution: any, fileName: string): Promise<void> {
        try {
            let messageId = await this.getLineMessageId(fileExecution, fileExecution.index);
            if (messageId !== 'Not Found' && messageId !== 'No More Lines') {
                if(await this.isActualMessageIdDiferent(executionMessage.message, messageId)) {
                    let messageDate = await this.getMessageDateFromLine(line, fileDate);
                    await this.createOrRetrieveMessage(messageId, executionMessage, messageDate, fileDate, fileName);
                }
                
                await this.getLineInformations(executionMessage, line, fileExecution);
            }
            return;
        } catch (ex) {
            throw new Error(ex);
        }
    }
    async processLineGetActivateResourceIn(line: string, executionMessage: any): Promise<void> {
        if (!executionMessage.message.activateResourceIn) {
            let activateResourceIn = line.match(/\<ActivateResourceIn\>.*\<\/ActivateResourceIn\>/gi);
            if (activateResourceIn && activateResourceIn.length > 0) {
                executionMessage.message.activateResourceIn = activateResourceIn[0];
                executionMessage.message.activateResourceInActivity = await this.getTag(['<name>'], executionMessage.message.activateResourceIn);
            }
        }
        return;
    }
    async processLineGetOrder(line: string, executionMessage: any): Promise<void> {
        if (!executionMessage.message.externalId) {
            let externalId = line.match(/(\<externalId\>.*\<\/externalId\>)|(\<purchaseOrderNumber\>.*\<\/purchaseOrderNumber\>)/gi);
            if (externalId && externalId.length > 0) {
                executionMessage.message.externalId = await this.getTag(['<externalId>', '<purchaseOrderNumber>'], externalId[0]);
            }
        }
        return;
    }
    async processLineGetRaConnectorIteraction(line: string, executionMessage: any, fileExecution: any): Promise<boolean> {
        let serviceRequest = line.match(/\<ServiceRequest\>.*\<\/ServiceRequest\>/ig);
        if (serviceRequest && serviceRequest.length > 0) {
            executionMessage.message.raConnectorIteraction = serviceRequest[0];
            return true;
        } else if (line.includes('Message: <?xml version="1.0"?>')) {
            let nextLine = fileExecution.lines[fileExecution.index + 1];
            if (nextLine.search(/(\<ServiceRequest\>)|(\<\/ServiceRequest\>)/ig) >= 0) {
                if (executionMessage.message.raConnectorIteraction && executionMessage.message.raConnectorIteraction.search(/\<ServiceRequest\>.*\<\/ServiceRequest\>/ig) >= 0) {
                    return true;
                } 

                let messageId;
                do {
                    fileExecution.index++;
                    nextLine = fileExecution.lines[fileExecution.index];
                    
                    if (nextLine.search(/\<ServiceRequest\>.*\<\/ServiceRequest\>/ig) >= 0) {
                        executionMessage.message.raConnectorIteraction = nextLine;    
                    } else if (!executionMessage.message.raConnectorIteraction && nextLine.search(/(\<ServiceRequest\>)/gi) >= 0) {
                        executionMessage.message.raConnectorIteraction = nextLine;
                    } else {
                        executionMessage.message.raConnectorIteraction += nextLine;
                    }

                    messageId = await this.getLineMessageId(fileExecution, fileExecution.index + 1);
                }
                while (messageId === 'Not Found')
                
                return true;
            }
        }
        return false;
    }
    async processLineGetNotification(line: string, executionMessage: any, fileExecution: any): Promise<boolean> {
        if (line.search('notify] :: NotifyResourceProvisioningIn') >= 0) {
            let notifications = line.match(/\<root.*\>.*<\/root>/ig);
            if (notifications && notifications.length > 0) {
                if (!executionMessage.message.notifications.includes(notifications[0])) {
                    executionMessage.message.notifications.push(notifications[0]);
                }
            } else if (line.search(/\<root.*\>/gi) >= 0) {
                let messageId = await this.getLineMessageId(fileExecution, fileExecution.index + 1);
                let notification = line;
                while (messageId === 'Not Found') {
                    fileExecution.index++;
                    notification += fileExecution.lines[fileExecution.index];
                    messageId = await this.getLineMessageId(fileExecution, fileExecution.index + 1);   
                }
                if (!executionMessage.message.notifications.includes(notification)) {
                    executionMessage.message.notifications.push(notification);
                }
            }
            return true;
        }
        return false;
    }
    async processLineGetCorrelations(line: string, executionMessage: any): Promise<void> {
        if (line.search('<sourceCorrelationId>') >= 0) {
            let sourceCorrelationId = await this.getTag(['<sourceCorrelationId>'], line);
            if (executionMessage.message.sourceCorrelationId && executionMessage.message.sourceCorrelationId != sourceCorrelationId) {
                throw new Error('More than one source correlation id');
            }
            executionMessage.message.activity = sourceCorrelationId;
        } else {
            let correlation = line.match(/(\<RequestNumber\>.*\<\/RequestNumber\>)|(\<correlationId\>.*\<\/correlationId\>)/gi);
            if (correlation && correlation.length > 0) {
                let correlationId = await this.getTag(['<RequestNumber>', '<correlationId>'], correlation[0]);
                if (correlationId != executionMessage.message.messageId) {
                    if(!executionMessage.message.correlationsMessagesIds.includes(correlationId)) {
                        executionMessage.message.correlationsMessagesIds.push(correlationId);
                    }
                } else {
                    executionMessage.message.isCorrelation = true;
                }
            }
        }
        return;
    }
    async processLineGetErrors(line: string, executionMessage: any, fileExecution: any): Promise<void> {
        let ossBusinessError = line.match(/\<ossBusinessException\>.*\<\/ossBusinessException\>/gi);
        if (ossBusinessError && ossBusinessError.length > 0) {
            executionMessage.message.errorsMessages.push(...ossBusinessError);
            return;
        } else if (line.search('<ossBusinessException>') >= 0) {
            let error = line;
            let messageId = await this.getLineMessageId(fileExecution, fileExecution.index + 1);
            while (messageId === 'Not Found') {
                fileExecution.index++;
                error += fileExecution.lines[fileExecution.index];
                messageId = await this.getLineMessageId(fileExecution, fileExecution.index + 1);
            }
            executionMessage.message.errorsMessages.push(error);
        } else if (await this.hasError(line) && !executionMessage.message.errorsMessages.includes(line)) {
            let error = line;
            let limit = 0;
            let messageId = await this.getLineMessageId(fileExecution, fileExecution.index + 1);
            while (messageId === 'Not Found' && limit < 5) {
                fileExecution.index++;
                error += '\n' + fileExecution.lines[fileExecution.index];
                messageId = await this.getLineMessageId(fileExecution, fileExecution.index + 1);
                limit++
            }
            executionMessage.message.errorsMessages.push(error);
        }
    }
    async processLineGetRequestOrAsyncResponse(line: string, executionMessage: any, fileExecution: any): Promise<boolean> {
        let request;
        let response;
        if (line.search(/(\<ns0\:originSystem\>ResourceActivation\<\/ns0:originSystem\>)|(GvoxRTBRequestWS \: \<\?xml)/ig) >= 0) {
            request = line.match(/\<root.*\>.*\<\/root\>/);
        } else if (line.search(/\<CallbackRequest\>/) >= 0) {
            request = line.match(/(\<CallbackRequest\>.*\<\/CallbackRequest\>)/gi)
        } else if (line.search(/\<responseEntity\>/gi) >= 0) {
            response = line.match(/(\<responseEntity\>.*\<\/responseEntity\>)/gi)
        } else {
            request = line.match(/(\<requestEntity\>.*\<\/requestEntity\>)/gi)
        } 

        if (response && response.length > 0) {
            if (!executionMessage.message.responses.includes(response[0])) {
                executionMessage.message.responses.push(response[0]);
            }
            return true;
        }

        if (request && request.length > 0) {
            if (!executionMessage.message.requests.includes(request[0])) {
                executionMessage.message.requests.push(request[0]);
            }
            return true;
        }
        return false;
    }
    async wasProcessed(process: any, file: any): Promise<void> {
        process.filesProcessed++;
        this.processedFiles++;
        this.processingFiles--;
        console.log('Processed log files: ' + (this.processedFiles/this.files.length * 100) + '%');
        if (this.currentFileIndex === this.processedFiles) {
            console.log('Processo completou.');
            process.finalDate = new Date();
            console.log('Cache was: ' + Object.keys(this.auditRepository.cache).length);
            await process.save();
            await this.reset();
            clearInterval(this.monitoring);
        } else {
            this.fileDone$.next(file.name);
        }
    }
}