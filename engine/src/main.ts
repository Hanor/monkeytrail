import {Application, Inject} from './configuration/noants/decorators';
import {NoAnTsConfiguration} from './configuration/noants/noandts.configuration';

import {UserRest} from './user/user.rest';
import {AuditRest} from './audit/audit.rest';
import { ServiceInventoryRest } from './service-inventory/service-inventory.rest';

@Application
export class Main {

  @Inject applicationContext: NoAnTsConfiguration;
  @Inject userRest: UserRest;
  @Inject auditRest: AuditRest;
  @Inject serviceInventoryRest: ServiceInventoryRest

  constructor(private args: Array<string>) {
    this.applicationContext.run(args);
  }
}
