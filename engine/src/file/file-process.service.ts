import * as extract from 'node-gzip';
import * as yauzl from 'yauzl';
import * as fs from 'fs';
import * as crypto from 'crypto'
import { Service } from '../configuration/noants/decorators';
import { AuditProcessFileModel } from '../audit/audit-process.entity';
import { AuditService } from '../audit/audit.service';


@Service
export class FileProcessService {
    maxDate: Date;
    minDate: Date;
    files: Array<any> = [];

    async getFilesNotCompleted() {
        return await AuditProcessFileModel.find({completed: false});
    }
    async getFileContent(file: string): Promise<any> {
        try {
            return await fs.readFileSync(file, 'UTF-8');
        } catch(ex) {
            return null;
        }
    }
    async getFilesFromPath(path: string): Promise<string[]> {
        return fs.readdirSync(path);
    }
    async checkFile(stats: fs.Stats, file: string, path: string, fromDate: Date) {
        if (stats.mtime < fromDate) {
            return;
        }

        let fullFileName = path + '/' + file;
        let hash: crypto.Hash = crypto.createHash('sha512');
        let fileDate = stats.mtime;
        let fileName = hash.update(path + '/' + file.replace(/(\.zip)|(\.gz)/g, '') + fileDate.getTime()).digest('hex');
        
        let processedFile = await AuditProcessFileModel.findOne({name: fileName})
        let actualLog = this.wasFileUpdatedToday(stats.mtime);
        
        if (processedFile && processedFile.moved && !actualLog) {
            return;
        } else if (processedFile) {
            this.files.push({url: fullFileName, name: file, finalNameFile: fileName});
            processedFile.completed = false;
            processedFile.moved = false;
            await processedFile.save();
        } else {
            if (!this.minDate || stats.mtime < this.minDate) {
                this.minDate = stats.mtime;
            }

            if (!this.maxDate || stats.mtime > this.maxDate) {
                this.maxDate = stats.mtime;
            }

            this.files.push({url: fullFileName, name: file, finalNameFile: fileName});
            let processedFile = new AuditProcessFileModel();
            processedFile.dateFile = stats.mtime;
            processedFile.name = fileName;
            processedFile.compressed = (file.match(/(\.zip)|(\.gz)/g)) ? true: false;
            processedFile.completed = false;
            processedFile.moved = false;
            await processedFile.save();
        }
                    
    }
    async checkFilesToBeProcessed(path: string, fromDate: Date) {
        path = path.replace(/-/g, '/');
        let dir = fs.readdirSync(path);
        for(let file of dir) {
            let stats = fs.statSync(path + '/' + file);
            if (stats.isDirectory()) {
                await this.checkFilesToBeProcessed(path + '/' + file, fromDate);
            } else if (!file.includes('access.log')) {
                await this.checkFile(stats, file, path, fromDate);
            }
        }
    }
    async extractAndMoveFiles(process: any) {
        for (let file of this.files) {
            console.log('Processing files to be extracted: ' + (process.filesProcessed/process.filesAmount * 100) + '%');
            
            let processedFile = await AuditProcessFileModel.findOne({name: file.finalNameFile})
            let data;
            try {
                if (file.name.search(/(\.zip)|(\.gz)/g) === -1) {
                    data = fs.readFileSync(file.url);
                    
                } else {
                    data = await this.extractFile(file.url);
                }

                fs.writeFileSync(AuditService.LOG_FILES + file.finalNameFile, data);
                processedFile.moved = true;
                processedFile.save();
            } catch (ex) {
                console.log("Erro ao processar arquivo: " + file)
                console.error(ex);
            } finally {
                process.filesProcessed++;
            }
        }
    }
    private async extractGz(file) {
        let content = fs.readFileSync(file);
        return await extract.ungzip(content);
    }
    private async extractGzAndZip(file) {
        let content = fs.readFileSync(file);
        let zipBuffer = await extract.ungzip(content);
        return new Promise((resolve, reject) => {
            let finalContent = '';
            yauzl.fromBuffer(zipBuffer, (err, zip) => {
                zip.on('entry', (entry) => {
                    zip.openReadStream(entry, (err, readStream) => {
                        if (err) throw err;
                        if (readStream.expectedByteCount === 0) {
                            resolve('');
                        }
                        readStream.on("data", (data) => {
                            finalContent += data;
                        });
                        readStream.on('end', () => {
                            resolve(finalContent);
                        })
                    });
                }).on('error', (error) => {
                    reject(error);
                })
            })
        })
    }
    private async extractZip(file) {
        let zipBuffer = fs.readFileSync(file);
        return new Promise((resolve, reject) => {
            yauzl.fromBuffer(zipBuffer, (err, zip) => {
                if (err) {
                    reject(err);
                }
                if (!zip) {
                    console.error(file);
                    reject(new Error("Por algum motivo não foi possível realizar o zip..."))
                }
                zip.on('entry', (entry) => {
                    zip.openReadStream(entry, (err, readStream) => {
                        if (err) {
                            console.log(err);
                        }
                        if (readStream.expectedByteCount === 0) {
                            resolve('');
                        }
                        readStream.on("data", (data) => {
                            resolve(data);
                        });
                        readStream.on("error", (err) => {
                            reject(err);
                        });
                    });
                }).on('error', (error) => {
                    reject(error);
                })
            })
        })
    }
    private async extractFile(file: string): Promise<any> {
        try {
            if (file.search(/zip.*\.gz/) > -1) {
                return await this.extractGzAndZip(file);
            } else if (file.includes('.zip')) {
                return await this.extractZip(file);
            } else if (file.includes('.gz')) {
                return await this.extractGz(file);
            } else {
                throw new Error('File not supported: ' + file);
            }
        } catch (ex) {
            throw ex;
        }
    }
    private wasFileUpdatedToday(fileDate: Date) {
        let auxDate = new Date(fileDate);
        let today = new Date();
        today.setHours(0,0,0,0);
        auxDate.setHours(0,0,0,0);

        return auxDate === today;
    }
}