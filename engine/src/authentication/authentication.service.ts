import { Service, Inject } from "../configuration/noants/decorators";
import * as JWT from 'jsonwebtoken';
import { UserService } from "../user/user.service";
import { UserEntity } from "../user/user.entity";

// create you secret key;
const SECRET = 'Secret like life'

@Service
export class AuthenticationService {

    @Inject userService: UserService;

    async doSignIn(login: string, password: string): Promise<any> {
        const user: UserEntity = await this.userService.getUserByUidAndPassword(login, password);
        if (!user) {
            throw new Error('Username or Password is not correct.');
        }
        const token: string = JWT.sign({data:JSON.stringify(user)}, SECRET,{ expiresIn: '3 days' });
        return {token: token};
    }
    doSignOut(username: string): any {
        return {signOut: true};
    }
    async isTokenValid(token: string) {
        try {
            const decoded = JWT.verify(token, SECRET);
            const tokenUser = JSON.parse(decoded.data);
            const currentUser: UserEntity = await this.userService.getUserByUid(tokenUser.id);
            if (tokenUser.username != currentUser.getUserName() || tokenUser.password != currentUser.getPassword()) {
                return false;
            } else {
                return true;
            }
        } catch (ex) {
            throw ex;
        }
    }
}