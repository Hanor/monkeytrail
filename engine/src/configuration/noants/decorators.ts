import 'reflect-metadata';
import {ServerConfiguration} from './server.configuration';
import {PropertiesConfiguration} from './properties.configuration';
import {BehaviorSubject} from 'rxjs';
import {NoAnTsConfiguration} from './noandts.configuration';
import { AuthenticationService } from '../../authentication/authentication.service';

let instances: Map<string, Object> = new Map<string, Object>();
let applicationContext: NoAnTsConfiguration;

export const READY: BehaviorSubject<boolean> = new BehaviorSubject(false);

export function Application(constructor: Function) {
  if (!instances.has('APPLICATION')) {
    const prototype = Object.create(constructor.prototype);
    let instance = new prototype.constructor(process.argv);
    instances.set('APPLICATION', instance);
  } else {
    throw Error('More than one Application main.');
  }
}
export function Rest(constructor: Function) {}
export function Repository(constructor: Function) {}
export function Service(constructor: Function) {}
export function Configuration(constructor: Function) {}
export function Inject(target: any, key: string) {
  const constructor = Reflect.getMetadata("design:type", target, key);
  let instance;
  if (instances.has(constructor.name)) {
    instance = instances.get(constructor.name);
  } else {
    const prototype = Object.create(constructor.prototype);
    instance = new prototype.constructor();
    instances.set(constructor.name, instance);
  }
  Reflect.set(target, key, instance);
}
export function Get(args: Array<any>) {
  let path: string = args[0];
  let auth: boolean = args[1];
  let role: string = args[2];
  return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
    READY.subscribe((ready) => {
      if (ready) {
        const serverConfiguration: ServerConfiguration = <ServerConfiguration> instances.get('ServerConfiguration');
        serverConfiguration.context.get(path, async (req, res) => {
          try {
            await verifyRoleAndToken(auth, role, req, res);
            if (path.includes(':')) {
              const result: any = await target[propertyKey](req.params[path.split(':')[1]]);
              res.send(result);
            } else {
              const result: any = await target[propertyKey](req.query);
              res.send(result);
            }
          } catch (ex) {
            console.error(ex);
            if (ex instanceof TokenNotFound || ex.constructor.name === 'TokenExpiredError') {
              res.status('401').send(ex.message);
            } else {
              res.status('500').send(ex.message);
            }
          }
        })
      }
    })
  };
}
export function Post(args: Array<any>) {
  let path: string = args[0];
  let auth: boolean = args[1];
  let role: string = args[2];
  return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
    READY.subscribe((ready) => {
      if (ready) {
        const serverConfiguration: ServerConfiguration = <ServerConfiguration> instances.get('ServerConfiguration');
        serverConfiguration.context.post(path, async (req, res) => {
          try {
            await verifyRoleAndToken(auth, role, req, res);
            res.send(await target[propertyKey](req.body));
          } catch (ex) {
            console.error(ex);
            if (ex instanceof TokenNotFound || ex.constructor.name === 'TokenExpiredError') {
              res.status('401').send(ex.message);
            } else {
              res.status('500').send(ex.message);
            }
          }
        })
      }
    })
  };
}


/*
*
* States
*/
async function verifyRoleAndToken(auth: boolean, role: string, req: any, res: any): Promise<void> {
  const authenticationService: AuthenticationService = <AuthenticationService> instances.get('AuthenticationService');
  if (auth) {
    await verifyToken(req, authenticationService);
  }
}
async function verifyToken(req: any, authenticationService: AuthenticationService): Promise<void> {
  let token = req.headers.authorization;
  if (token) {
    token = token.replace('Bearer ', '');
    await authenticationService.isTokenValid(token);
  } else {
    throw new TokenNotFound('Token not found.');''
  }
}

class TokenNotFound extends Error {
  constructor(message: string) {
    super(message)
  }
}