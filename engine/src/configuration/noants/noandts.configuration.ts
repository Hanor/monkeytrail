import {ServerConfiguration} from './server.configuration';
import {PropertiesConfiguration} from './properties.configuration';
import {BehaviorSubject} from 'rxjs';
import {Inject, READY} from './decorators'
import { DatabaseConfiguration } from './database.configuration';

export class NoAnTsConfiguration {
  @Inject propertiesConfiguration: PropertiesConfiguration;
  @Inject serverConfiguration: ServerConfiguration;
  @Inject databaseConfiguration: DatabaseConfiguration;

  constructor(){}

  run(args: Array<string>) {
    if (!READY.getValue()) {
      this.propertiesConfiguration.loadPropertes(null);
      this.serverConfiguration.configure(this.propertiesConfiguration.getServerIp(), this.propertiesConfiguration.getServerPort(), this.propertiesConfiguration.getFilePath());
      this.databaseConfiguration.configure(this.propertiesConfiguration.getDatabaseUri(), this.propertiesConfiguration.getDatabaseName());
      READY.next(true);
    }
  }
}
