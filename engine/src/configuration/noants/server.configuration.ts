import {PropertiesConfiguration} from './properties.configuration'
import * as express from 'express';
import * as morgan from 'morgan';
import * as bodyParser from 'body-parser';
import * as methodOverride from 'method-override';
import * as StrongSoap from 'strong-soap';
import * as Ldap from 'ldapjs';
import { Inject, Configuration } from './decorators';

@Configuration
export class ServerConfiguration {

  @Inject propertiesConfiguration: PropertiesConfiguration;
  
  ip: string;
  context: any;
  filesPath: string;
  port: number;
  ldapClient: any;
  serviceInventoryQa3: any;
  serviceInventoryProd: any;

  constructor() {};

  async configure(ip: string, port: number, filesPath: string) {
    try {
      await this.configureApi(ip, port, filesPath);
      await this.configureLdap();
      await this.configureSoap();
    } catch(ex) {
      console.error(ex);
    } finally {
      this.start();
    }
  }
  async configureApi(ip, port: number, filesPath: string) {
    this.filesPath = filesPath;
    this.port = port;
    this.ip = ip;
    this.context = express();
    this.context.use(express.static( this.filesPath ));
    this.context.use(morgan('dev'));
    this.context.use(bodyParser.urlencoded({'extended': true}));
    this.context.use(bodyParser.json());
    this.context.use(bodyParser.json({ type: 'application/vnd.api+json' }));
    this.context.use(methodOverride());
  }
  async configureLdap(): Promise<void> {
    try {
      this.ldapClient = Ldap.createClient({url: this.propertiesConfiguration.getLdapUri(), reconnect: true});
      this.ldapClient.on('error', (err) => {
        console.error('Not connected');
        console.error(err);
      })
      
    } catch (ex) {
      return Promise.reject(ex);
    }
  }
  async configureSoap(): Promise<void> {
    let qatres = this.propertiesConfiguration.getServiceInventoryQa3();
    let prod = this.propertiesConfiguration.getServiceInventoryProd();
    let self = this;
    try {
      await StrongSoap.soap.createClient(qatres, {}, async function(err, client) {
        if (err) {
          console.error(err);
        }
        self.serviceInventoryQa3 = client;
      })
      await StrongSoap.soap.createClient(prod, {}, async function(err, client) {
        if (err) {
          console.error(err);
        }
        self.serviceInventoryProd = client;
      })
    } catch(ex) {
      console.error(ex);
    }
  }
  initializeAngularRoutes() {
    this.context.get('/*', (req, res, next) => {
      if (!req.path.includes('/api/')) {
        res.sendFile(this.filesPath + '/index.html', { root: '.' });
      }
    })
  }
  start() {
      this.context.listen(this.port, this.ip, () => {
          console.log(`Listening at http://localhost:${this.port}/`);
          this.initializeAngularRoutes();
      })
  }
}
