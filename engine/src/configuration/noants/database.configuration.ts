import { Configuration } from './decorators';
import * as mongoose from 'mongoose'

@Configuration
export class DatabaseConfiguration {
    connection: any;
    configure(uri: string, name: string) {
        mongoose.connect(uri + name, { useNewUrlParser: true });
        mongoose.Promise = global.Promise;
        this.connection = mongoose.connection;
        this.connection.on('error', console.error.bind(console, 'MongoDB connection error:'));
    }
}