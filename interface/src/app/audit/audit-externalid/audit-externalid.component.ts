import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { AuditService } from '../audit.service';
import { FormBuilder } from '@angular/forms';
import { Subscription, BehaviorSubject } from 'rxjs';

@Component({
  selector: 'audit-externalId',
  templateUrl: './audit-externalid.component.html',
  styleUrls: ['./audit-externalid.component.scss']
})
export class AuditExternalIdComponent implements OnInit, OnDestroy {

    @Input('externalId') externalId: any;
    @Input('messageId') message: any;
    @Input('messagePanel') messagePanel: any;

    subscriptions$: Subscription = new Subscription();
    correlations$: BehaviorSubject<any> = new BehaviorSubject(null);

    constructor(
        private auditService: AuditService
    ) {}

    ngOnInit(): void {}
    ngOnDestroy(): void {
        this.subscriptions$.unsubscribe();
    }
    eventMessageExpand(): void {
        if (this.messagePanel.message != this.message) {
            this.messagePanel.message = this.message; 
            this.getCorrelationsMessages();
        }
    }
    parseDate(date: Date) {
        date = new Date(date);
        let day = date.getDate();
        let month = date.getMonth() + 1;
        let year = date.getFullYear();
        let hours = date.getHours();
        let minutes = date.getMinutes();
        let seconds = date.getSeconds();
        return day + '/' + month + '/' + year + ' ' + hours + ':' + minutes + ':' + seconds;
    }
    getActivityFromActivateResourceIn(activateResourceIn) {
        if (activateResourceIn) {
            let key = '<name>';
            let endKey  = '</name>';
            let min = activateResourceIn.search(key);
            let max = activateResourceIn.search(endKey);
            if (min >= 0 && max > min) {
                let actionName = activateResourceIn.substring(min + key.length, max).replace(/\_/g, ' ').toLowerCase();
                return actionName.charAt(0).toUpperCase() + actionName.slice(1);
            } else {
                console.warn('Activate Resource In havent the action name!!');
            }
        }
        return 'Sem atividade'
    }
    getCorrelationsMessages(): void {
        let correlations = this.message.correlationsMessagesIds.filter((correlation) => {
            return !correlation.includes('Fulfillment')
        })
        if (correlations.length > 0) {
            this.auditService.findMessagesByMessageId(correlations).subscribe(this.correlations$);
        }
    }
}
