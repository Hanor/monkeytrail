import { NgModule } from '@angular/core';
import { AuditComponent } from './audit.component';
import { AppAngularMaterialModule } from '../app-angular.material.module';
import { AuditExternalIdComponent } from './audit-externalid/audit-externalid.component';


@NgModule({
  declarations: [
    AuditComponent,
    AuditExternalIdComponent
  ],
  imports: [
    AppAngularMaterialModule
  ],
  exports: [AuditComponent, AuditExternalIdComponent]
})
export class AuditModule {}