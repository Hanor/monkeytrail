import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuditService } from './audit.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Subscription, Observable, BehaviorSubject } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'audit',
  templateUrl: './audit.component.html',
  styleUrls: ['./audit.component.scss']
})
export class AuditComponent implements OnInit, OnDestroy {
    correlations$: BehaviorSubject<any> = new BehaviorSubject(null);

    spinner: boolean = false;

    search: FormGroup;
    externalIds: Array<any> = [];
    subscriptions$: Subscription = new Subscription();

    constructor(
        private auditService: AuditService,
        private router: Router,
        private route: ActivatedRoute,
        private formBuilder: FormBuilder
    ) {}

    ngOnInit(): void {
        this.search = this.formBuilder.group({
            externalId: ['', Validators.required],
            activities: [''],
            getAll: [null]
        })
        this.eventUrlChange();
        // this.auditService.getServiceInventoryByDesignator('29752123805', 'qa3');
    }
    ngOnDestroy(): void {
        this.subscriptions$.unsubscribe();
    }
    auditExternalIds(query: any) {
        let request:any = {externalId : Array.from(new Set(query.externalId.split(/\,| /g)))}
        let activities = (query.activities) ? Array.from(new Set(query.activities.split(/\,| /g))): [];
        
        if (!query.getAll || query.getAll === 'false') {
            request.correlation = false;
        }
        if (activities.length > 0 && activities[0] !== '') {
            request.activities = activities;
        }

        this.spinner = true;
        this.subscriptions$.add(this.auditService.auditExternalIds(request).subscribe((externalIds) => {
            this.externalIds = externalIds
            this.spinner = false;
        }, (error: any) => {
            this.spinner = false;
            console.error(error);
        }))
    }
    eventUrlChange() {
        this.subscriptions$.add(this.route.queryParams.subscribe((query) => {
            if (query && query.externalId) {
                this.auditExternalIds(query);
            } else {
                this.externalIds = [];
            }

            for (let key of Object.keys(query)) {
                if (this.search.get(key).value !== query[key]) {
                    let value = query[key].toString();
                    if (key === 'getAll' && value === 'false') {
                        value = null;
                    } 
                    this.search.get(key).setValue(value);
                }
            }
        }))
    }
    eventAudit(): void { 
        this.router.navigate(['.'], {relativeTo: this.route, queryParams: this.search.getRawValue()});
    }
    async eventReport() {
        await this.auditService.generateReport(this.externalIds);
    }
    foundExternalIds() {
        let count = 0.
        for (let externalid of this.externalIds) {
            if (externalid.messages.length > 0) {
                count++;
            }
        }
        return count;
    }
    getCorrelationsMessages(message: any): void {
        let correlations = message.correlationsMessagesIds.filter((correlation) => {
            return !correlation.includes('Fulfillment')
        })
        if (correlations.length > 0) {
            this.auditService.findMessagesByMessageId(correlations).subscribe(this.correlations$);
        }
    }
}
