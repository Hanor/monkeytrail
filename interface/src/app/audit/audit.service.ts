import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserModel } from '../shared/models/user.model';
import { BehaviorSubject, Observable } from 'rxjs';
 
@Injectable({ providedIn: 'root' })
export class AuditService {
    currentUser$: BehaviorSubject<UserModel> = new BehaviorSubject(null);
    constructor(
        private http: HttpClient
    ) {}

    auditExternalIds(orders: any): Observable<any> {
        return this.http.post<any>('/api/auditExternalIds', orders);
    }
    findMessagesByMessageId(ids: Array<string>): Observable<any> {
        return this.http.get<any>('/api/findMessagesByMessageIds', {params: {messagesIds: ids}});
    }
    getServiceInventoryByDesignator(designator: string, environment: string): Observable<any> {
        return this.http.get<any>('/api/getServiceInventoryByDesignator?designator=' + designator +'&environment=' + environment);
    }
    
    async generateReport(externalIds: Array<any>) {
        let csv = '\ufeff ORDEM;DATA;ATIVIDADE;AMBIENTE;EQUIPAMENTO;ANALISADO POR;DEVE SER ANALISADO POR;OBSERVAÇÃO;ERRO;REQUEST;RESPONSE\n';
        for (let externalId of externalIds) {
            for (let message of externalId.messages) {
                let owner: string = '';
                let request: string = '';
                let erro = '';
                let observation = '';
                let response = '';
                let ambiente = false;
                let data = message.date;
                let equipamento = '';
                if (message.errorsMessages.length === 0) {
                    continue;
                }

                if (message.activateResourceInActivity === 'CONFIGURE_AUTHENTICATION' && new Date(message.date) < new Date(2018, 10, 7)) {
                    continue;
                }

                for (let error of message.errorsMessages) {
                    error = error.replace(/\n/gi, ' ');
                    if (error.includes("10.23.235.146, 8081") || error.includes('com.tlf.oss.common.exception.OSSException: Erro na execução do rest -> java.io.EOFException: Response had end of stream after') || error.includes('Erro ao chamar o PSA -> 6 - NRC already in use') || error.includes('[RAPSA-005] :: Erro ao chamar o PSA -> 5 - Client not found')) {
                        owner = 'Engenharia';
                        erro = 'BUG 126'
                        if (error.includes("10.23.235.146, 8081")) {
                            observation = 'Não foi possível conectar ao servidor: 10.23.235.146, 8081';
                        } else if (error.includes('[RAPSA-005] :: Erro ao chamar o PSA -> 5 - Client not found')) {
                            observation = 'Cliente já foi liberado.';
                        } else if (error.includes(' Erro ao chamar o PSA -> 6 - NRC already in use')) {
                            observation = 'Cliente está em uso.';
                        } else {
                            observation = 'Conexão caiu enquanto estava processando a resposta/chamada';
                        }
                        ambiente = true;
                        request = message.requests[0];
                        break;
                    } else if (error.includes('Erro oriundo do equipamento/plataforma') || error.includes('Erro+oriundo+do+equipamento')) {
                        observation = 'Erro na execução da chamada ao Conector';
                        request = message.raConnectorIteraction;
                        let correlationsId  = [];
                        for (let correlation of message.correlationsMessagesIds) {
                            if (!correlation.includes('Fulfillment')) {
                                correlationsId.push(correlation);
                            }
                        }
                        let correlations = await this.findMessagesByMessageId(correlationsId).toPromise();
                        if (correlations.length > 0) {
                            for(let correlation of correlations) {
                                if (correlation.raConnectorIteraction) {
                                    response += correlation.raConnectorIteraction + '         ';
                                    if (correlation.raConnectorIteraction.search('No Response from Device') >= 0) {
                                        owner = 'Rodrigo/Priscila';
                                        erro = 'Timeout de equipamento';
                                        ambiente = true;
                                    } else if (correlation.raConnectorIteraction.search('Failure: VLAN does not exist') >= 0) {
                                        owner = 'Engenharia';
                                        erro = 'Vlan deve estar cadastrada previamente';
                                    } else if (correlation.raConnectorIteraction.search('Equipamento em uso, tente em alguns') >= 0) {
                                        owner = 'Rodrigo/Priscila';
                                        erro = 'Equipamento em uso';
                                        ambiente = true;
                                    } else if (correlation.raConnectorIteraction.search('Perfil nao cadastrado no DSLAM') >= 0) {
                                        owner = 'Engenharia';
                                        erro = 'Perfil nao cadastrado no DSLAM';
                                    } else {
                                        let errorDesc = correlation.raConnectorIteraction.match(/\<ComponentHeader\>.*\<\/ComponentHeader\>/gi);
                                        if (errorDesc && errorDesc.length > 0) {
                                            errorDesc = errorDesc[0].match(/\<Desc\>.*\<\/Desc\>/gi)
                                        }

                                        if (errorDesc && errorDesc.length > 0) {
                                            owner = 'Rodrigo/Priscila';
                                            erro = errorDesc;
                                        } else {
                                            owner = 'Hanor';
                                            erro = 'Erro sem descriçao, necessário analisar.'  
                                        }
                                    }
                                } else if (correlation.errorsMessages && correlation.errorsMessages.length > 0) {
                                    for (let error of correlation.errorsMessages) {
                                        response += error + '       ';
                                    }
                                } else {
                                    response += 'Não achou a resposta.'
                                }
                            }
                        } else {
                            response += 'Aparentemente não houve callback.';
                        }
                        if (owner === '') {
                            owner = 'Rodrigo/Priscila';
                            erro = 'Erro oriundo equipamento';
                        }
                        equipamento = message.activateResourceIn.match(/\<name\>EQUIPMENT_NAME\<\/name\>\<value\>.*\<\/value\>/gi)[0].match(/\<value\>.*\<\/value\>/)[0].replace(/(\<value\>)|(\<\/value\>.*)/gi, '');
                        break;
                    } else if (error.includes('Erro ao mapear objeto de configuração do PSA -> Não foi encontrado o parametro NRC')) {
                        owner = 'Paavo';
                        erro = 'Não veio o NRC'
                        observation =  '';
                        request = message.activateResourceIn;
                        break;
                    } else if (error.includes('Erro ao configurar TDM')) {
                        owner = 'Priscila/Rodrigo';
                        erro = 'GVOX - Erro ao configurar TDM'
                        request = message.requests[0];
                        break;
                    } else if (error.includes('Erro ao validar parametros -> Valor do EQUIPMENT_NAME')) {
                        owner = 'Paavo';
                        erro = 'Erro ao validar parametros -> Valor do EQUIPMENT_NAME eh null ou vazio!'
                        observation =  '';
                        request = message.activateResourceIn;
                        break;
                    } else if (error.includes('getSingleResult() did not retrieve any entities')) {
                        owner = 'Duran';
                        observation = 'Não foi possível recuperar item do banco de dados.';
                        erro = 'Não achou informação no banco de dados.';
                        request = message.activateResourceIn;
                        break;
                    } else if (error.includes('WebSphere MQ classes for JMS attempted to set a pre-existing client ID on a Connection or JMSContext') || error.includes('Erro para postar na fila MQ') || error.search(/ConfigureVoiceBridgeService \- Falha\: WebMethod postConfigureVoiceBridge \= \[null\] java\.lang\.NullPointerException\: null/gi) > -1 || error.search(/executar método execute da classe CreateProfileCommand\! Message\=\[null\] java\.lang\.NullPointerException\: null/gi) > -1) {
                        owner = 'Produção';
                        erro = 'Erro na fila MQ';
                        observation = 'Erro na fila MQ'
                        ambiente = true;
                        request = message.activateResourceIn;
                        break;
                    } else if (error.search('Error in operation : notifyResourceProvisioning') > -1) {
                        owner = 'Produção';
                        observation = 'Erro ao notificar o savvion';
                        erro = 'Erro ao notificar o savvion';
                        ambiente = true;
                        for(let notification of message.notifications) {
                            request += notification + '     ';
                        }
                        break;
                    } else if (error.includes('Erro ao configurar PTS')) {
                        if (message.notifications.length > 1) {
                            let correlationsId  = [];
                            for (let correlation of message.correlationsMessagesIds) {
                                if (!correlation.includes('Fulfillment')) {
                                    correlationsId.push(correlation);
                                }
                            }
                            let correlations = await this.findMessagesByMessageId(correlationsId).toPromise();
                            for(let correlation of correlations) {
                                if (correlation.requests.length > 1) {
                                    observation = 'Mais de uma resposta da engenharia.';
                                } 

                                for (let correlationResponse of correlation.requests) {
                                    response += correlationResponse + '     ';
                                }
                            }
                        }
                        if (observation === '') {
                            observation = 'Erro ao configurar PTS';    
                        }

                        owner = 'Engenharia';
                        erro = 'Erro ao configurar PTS';
                        for (let messageRequest of message.requests) {
                            request += messageRequest + '  ';
                        }
                        break;
                    } else if (error.includes('Erro ao validar parametros -> Valor do SAS_ACTION')) {
                        owner = 'Paavo';
                        erro = 'Erro ao validar parametros -> Valor do SAS_ACTION'
                        observation =  '';
                        request = message.activateResourceIn;
                        break;
                    }
                }
                if (erro === '') {
                    owner = 'Hanor';
                    erro = 'Não conhecido.';
                    observation = 'Necessário analisar melhor o log';
                    request = message.activateResourceIn;
                }
                csv += externalId.externalId + ';' + data + ';' + message.activateResourceInActivity + ';'+ ambiente + ';' + equipamento + ';' +';Hanor;'+ owner + ';'+ observation + ';' + erro + ';' + request + ';' + response +'\n'
            }
        }


        let a = document.createElement("a");
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        let blob = new Blob([csv], { type: 'text/csv' });
        let url = window.URL.createObjectURL(blob);
        a.href = url;
        a.download = 'report.csv';
        a.click();
        window.URL.revokeObjectURL(url);
        a.remove();
    }
}